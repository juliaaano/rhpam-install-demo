#!/bin/bash

set -euxo pipefail

if [ -z ${EAP_HOME+x} ]; then
	ERROR="EAP_HOME not set"
	exit 1
fi

PROPERTIES=postgres.properties

${EAP_HOME}/bin/jboss-cli.sh --properties=${PROPERTIES} --file=postgres-datasource.txt
${EAP_HOME}/bin/jboss-cli.sh --properties=${PROPERTIES} --file=postgres-kieserver.txt

SUCCESS="end of script"
